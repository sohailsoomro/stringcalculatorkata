﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using StringCalculatorKata;

namespace StringCalculatorKataTests
{
	[TestFixture]
	public class CalculatorTests
	{
		private Calculator _classUnderTest;

		[SetUp]
		public void Setup()
		{
			_classUnderTest = new Calculator();
		}

		[Test]
		public void ReturnsZeroForEmptyString()
		{
			var actual = _classUnderTest.Add(string.Empty);

			Assert.AreEqual(0, actual);
		}

		[TestCase("1", 1)]
		[TestCase("2", 2)]
		public void ReturnsNumber_WhenNotSeprated(string input, int expected)
		{
			var actual = _classUnderTest.Add(input);

			Assert.AreEqual(expected, actual);
		}

		[TestCase("1,2", 3)]
		[TestCase("4,2", 6)]
		public void ReturnsSum_WhenSeprated(string input, int expected)
		{
			var actual = _classUnderTest.Add(input);

			Assert.AreEqual(expected, actual);
		}

		[TestCase("1,2,3", 6)]
		[TestCase("1,1,1,1,1,1,1", 7)]
		[TestCase("1,5,6,7", 19)]
		public void CanAddUnknownAmountOfNumbers(string input, int expected)
		{
			var actual = _classUnderTest.Add(input);

			Assert.AreEqual(expected, actual);
		}

		[TestCase("1\n2,3,\n4", 10)]
		[TestCase("1\n2,3", 6)]
		public void HandlesNewLineBetweenNumbers(string input, int excpected)
		{
			var actual = _classUnderTest.Add(input);

			Assert.AreEqual(excpected, actual);
		}

		[TestCase("//;\n1;2", 3)]
		public void SupportsDifferentDelimiters(string input, int expected)
		{
			var actual = _classUnderTest.Add(input);

			Assert.AreEqual(expected, actual);
		}

		[TestCase("1,-2", "-2")]
		public void NegativeNumbersThrowException(string input, String expected)
		{
			var actual = Assert.Throws<Exception>(() => _classUnderTest.Add(input));

			Assert.AreEqual("Negatives not allowed " + expected, actual.Message);
		}

		[TestCase("1,-2,-3", "-2 -3")]
		public void MultipleNegativeNumbersAreDisplayedInErrorMessage(string input, string expected)
		{
			var actual = Assert.Throws<Exception>(() => _classUnderTest.Add(input));

			Assert.AreEqual("Negatives not allowed " + expected, actual.Message);
		}

		[TestCase("2,1001", 2)]
		public void IgnoreNumbersGreaterThan1000(string input, int expected)
		{
			var actual = _classUnderTest.Add(input);

			Assert.AreEqual(expected, actual);
		}

		[TestCase("//[***]\n1***2***3", 6)]
		[TestCase("//[$$$]\n1$$$10$$$3", 14)]
		public void DelimitersCanBeAnyLength_WhenWithinBracketsAndNewLineAfter(string input, int expected)
		{
			var actual = _classUnderTest.Add(input);

			Assert.AreEqual(expected, actual);
		}

		[TestCase("//[*][%][$]\n1*2%3", 6)]
		public void HandlesMultipleDelimiters(string input, int expected)
		{
			var actual = _classUnderTest.Add(input);

			Assert.AreEqual(expected, actual);
		}

		[TestCase("//[**][%$%]\n1**2%$%3", 6)]
		public void HandlesMultipleDelimiters_WithLengthGreaterThenOneCharacter(string input, int expected)
		{
			var actual = _classUnderTest.Add(input);

			Assert.AreEqual(expected, actual);
		}
	}
}
