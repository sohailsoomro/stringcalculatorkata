﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringCalculatorKata
{
	public class Calculator
	{
		public int Add(string stringNumbers)
		{
			int returnValue = 0;

			if (stringNumbers.Length == 0)
			{
				return 0;
			}

			stringNumbers = CheckDelimiters(stringNumbers);

			returnValue = SumNumbers(stringNumbers);

			return returnValue;
		}

		private static string CheckDelimiters(string numbers)
		{
			if (numbers.IndexOf("//") >= 0)
			{
				var delimiters = numbers.Substring(2, numbers.IndexOf("\n") - 2);

				numbers = RemoveCharacters(numbers, "\n");
				numbers = RemoveCharacters(numbers, "//");

				if (delimiters.IndexOf("[") >= 0)
				{
					numbers = RemoveDelimitersWithComma(numbers, delimiters);
				}
				else
				{
					numbers = ReplaceWithComma(numbers, delimiters);
				}
			}
			else
			{
				numbers = ReplaceWithComma(numbers, "\n");
			}

			return numbers;
		}

		private static string RemoveDelimitersWithComma(string numbers, string delimiters)
		{
			var numberOfDelimiters = delimiters.Count(x => x == '[');

			int startPosition = 0;
			int endPosition = 0;
			string delimiterWithBrackets = "";
			string delimiter = "";

			for (int i = 0; i < numberOfDelimiters; i++)
			{
				startPosition = delimiters.IndexOf("[", endPosition);
				endPosition = delimiters.IndexOf("]", startPosition);

				delimiterWithBrackets = delimiters.Substring(startPosition, (endPosition - startPosition) + 1);

				numbers = RemoveCharacters(numbers, delimiterWithBrackets);

				delimiter = RemoveCharacters(delimiterWithBrackets, "[");
				delimiter = RemoveCharacters(delimiter, "]");

				numbers = ReplaceWithComma(numbers, delimiter);
			}

			return numbers;
		}

		private static string RemoveCharacters(string stringNumbers, string removeCharacters)
		{
			return stringNumbers.Replace(removeCharacters, "");
		}

		private static string ReplaceWithComma(string stringNumbers, string delimiter)
		{
			return stringNumbers.Replace(delimiter, ",");
		}

		private int SumNumbers(string stringNumbers)
		{
			int sum = 0;
			string inValidNumbers = "";

			foreach (var stringNumber in stringNumbers.Split(','))
			{
				int number;

				if (int.TryParse(stringNumber, out number))
				{
					if (IsNegative(number))
					{
						inValidNumbers += stringNumber + " ";
					}

					if (!IsGreaterThan1000(number))
					{
						sum += number;
					}
				}
			}

			if (!string.IsNullOrWhiteSpace(inValidNumbers))
			{
				throw new Exception("Negatives not allowed " + inValidNumbers.Trim());
			}

			return sum;
		}

		private static bool IsNegative(int number)
		{
			return (number < 0);
		}

		private static bool IsGreaterThan1000(int number)
		{
			return (number > 1000);
		}
	}
}
